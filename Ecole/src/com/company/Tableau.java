package com.company;

import java.util.Scanner;

public class Tableau{

	int largeur;
	int longueur; 
	String couleur;

    public Tableau(int largeur, int longueur, String couleur) {
      	this.largeur = largeur ; 
      	this.longueur = longueur ; 
      	this.couleur = couleur; 
    }

	public void monTableau() {

		System.out.print("Ecrivez le contenu de votre tableau : \n");
		Scanner scanner = new Scanner(System.in);
		String c = scanner.nextLine();
		if (c.equals("")){
			this.monTableau();
		}
		else {
			System.out.println(this.toString());
			System.out.println("Le contenu de votre tableau est :");
			System.out.println(c);

		}
	}

    public String toString(){
    	String res =  "Le Tableau mesure : "+longueur + " cm de longueur "+ "et "+largeur+ " cm de largeur "+ "et de Couleur : "+ couleur;
    	return res; 
    }
	
}