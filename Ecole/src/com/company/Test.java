package com.company;

import java.util.*; 

public class Test {


	public static void main( String args[]){

		// Premiere salle de cours
		Tableau t1 = new Tableau(200, 200, "Noire");
		Cours c1 = new Cours(t1);

		List<Cours> sallesDeCours = new ArrayList<Cours>();
		sallesDeCours.add(c1);

		Ecole efficom = new Ecole("Efficom", sallesDeCours);

		efficom.sallesDeCours.get(0).monTableau();
		
	}
}