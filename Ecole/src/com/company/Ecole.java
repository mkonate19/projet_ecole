package com.company;
import java.util.*;

public class Ecole {
	String nom; 
	List<Cours> sallesDeCours = new ArrayList<Cours>();

	public Ecole(String nom, List<Cours> sallesDeCours) {
		this.nom = nom;
		this.sallesDeCours = sallesDeCours;
	}
}